//var mongoose = require("mongoose"),
Alumno = require("../models/alumnos");

//// cargar todos los alumnos
exports.loadAll = async (req, res, next) => {
    try {
        const alumno = await Alumno.find();
        if (req.isApi) {
            res.json(alumno);
        } else {
            return alumno;
        }
    } catch (error) {
      console.log(error);
      //Puede ser otro tipo de respuesta
    } 
}