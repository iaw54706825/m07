//var mongoose = require("mongoose"),
Asignatura = require("../models/asignatura");

//// cargar todas las asignaturas
exports.loadAll = async (req, res, next) => {
    try {
        const asignatura = await Asignatura.find();
        if (req.isApi) {
            res.json(asignatura);
        } else {
            return asignatura;
        }
    } catch (error) {
      console.log(error);
      //Puede ser otro tipo de respuesta
    } 
}

//cargar una asignatura
exports.loadOne = async (req, res, next) => {
    try {
        const asignatura = await Asignatura.findById(req.params.id);
        if(req.isApi) {
            res.json(asignatura);
        } else {
            return asignatura;
        }
    } catch (error) {
        console.log(error); 
    } 
}

// guardar asignatgura
exports.save = async (req, res, next) => {
    const asignatura = new Asignatura({
        nombre: req.body.nombre,
        numHoras: req.body.numHoras,
        docente: "http://127.0.0.1:3000/api/docentes/" + req.body.docente,
        alumno: "http://127.0.0.1:3000/api/docentes/" + req.body.alumno
    })
    try {
        const guardado = await asignatura.save();
        if (req.isApi) {
            res.status(201).json(guardado);
        } else {
            return guardado;
        }
    } catch (error) {
        console.log(error);
    }
}

//Eliminar asignatura
exports.delete = async (req, res) => {
    try {
        const asignatura = await Asignatura.findById(req.params.id);
        const resultado = asignatura.delete();
        if (req.isApi) {
            res.json({ mesagge: "Eliminado"});
        } else {
            return "Eliminado";
        }        
    } catch (error) {
        console.log(error); 
    } 
}

//Actualizar asignatura
exports.update = async (req,res) => {
    const asignatura = {
        nombre: req.body.name,
        numHoras: req.body.numHoras,
        docente: "http://127.0.0.1:3000/api/docentes/" + req.body.docente,
        alumno: "http://127.0.0.1:3000/api/docentes/" + req.body.alumno
    }
    try {
        const actu = await Asignatura.updateOne({_id: req.params.id}, asignatura)
        if(req.isApi) {
            res.status(200).json({ mesagge: "Actualizado"});
        } else {
            return "Actualizado";
        }
    } catch (error) {
        
    }
}