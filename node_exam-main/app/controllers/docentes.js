//var mongoose = require("mongoose"),
Docente = require("../models/docente");

//// cargar todas los docentes
exports.loadAll = async (req, res, next) => {
    try {
        const docente = await Docente.find();
        if (req.isApi) {
            res.json(docente);
        } else {
            return docente;
        }
    } catch (error) {
      console.log(error);
      //Puede ser otro tipo de respuesta
    } 
}