//Modelo
var mongoose = require('mongoose');
Schema = mongoose.Schema;

var AlumnoSchema = new mongoose.Schema({
    nombre: { type: String },
    apellido: { type: String },    
});

module.exports = mongoose.model("Alumno", AlumnoSchema);