var express = require("express");
var router = express.Router();
var path = require("path");
var ctrlDir = "/app/app/controllers";
var alumnrCtrl = require(path.join(ctrlDir, "alumnos"));
var docenteCtrl = require(path.join(ctrlDir, "docentes"));
var asignaturaCtrl = require(path.join(ctrlDir, "asignaturas"));


router.get("/asignatura/new", async function(req, res, next) {
    alumnos = await alumnrCtrl.loadAll(req);
    docentes = await docenteCtrl.loadAll(req);  
    res.render("newAsignatura",{listaAlumnos: alumnos, listaDocentes: docentes})
});

router.post("/asignatura/save", async function(req, res, next) {
    result = await asignaturaCtrl.save(req);
    if (result) {
        res.send("Asignatura añadida");    
    } else {
        res.send("No se ha añadido");
    }
});

module.exports = router;

