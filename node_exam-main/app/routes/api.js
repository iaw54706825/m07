var express = require("express");
var router = express.Router();
//Controladores
var path = require("path");
var ctrlDir = "/app/app/controllers";
var asignaturaCtrl = require(path.join(ctrlDir, "asignaturas"));



router.get("/asignaturas", asignaturaCtrl.loadAll);
router.post("/asignaturas", asignaturaCtrl.save);
module.exports = router;