exports.getAll = (req, res, next) => {
    var incidencias = [
        {
            id: 1,
            tipo: "tutorial borrado",
            descripcion: "prueba",
            user: 2
        },
        {
            id: 2,
            tipo: "recuperar contraseña",
            descripcion: "recuperar",
            user: 1
        }
    ];
    return res.json(incidencias);
}

exports.getIncidencias = (req, res, next) => {
    var incidencias = {
        id: 1,
        tipo: "tutorial borrado",
        descripcion: "prueba",
        user: 2
    }
    return res.json(incidencias);
};

exports.search = (req, res, next) => {
    var incidencias = {
        id: 1,
        tipo: "tutorial borrado",
        descripcion: "prueba",
        user: 2
    }
    return res.json(incidencias);
};

exports.new = (req, res, next) => {
    return res.json("false")
};

exports.delete = (req, res, next) => {
    return res.json("true");
}

exports.update = (req, res, next) => {
    return res.json("false");
};