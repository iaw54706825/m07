exports.getAll = (req, res, next) => {
  var user = [
    {
      id: 1,
      username: "Carlos",
      dni: "1234x",
      email: "carlos@gmail.com",
      rol: "alumno"
    },
    {
      id: 2,
      nombre: "Jacint",
      dni: "1234x",
      email: "carlos@gmail.com",
      rol: " teacher"
    }
  ];
  return res.json(user);
}

exports.getUser = (req, res, next) => {
  var user = {
    id: 1,
    username: "Carlos",
    dni: "1234x",
    email: "carlos@gmail.com",
    rol: "alumno"
  }
  return res.json(user);
};

exports.search = (req, res, next) => {
  var user = {
    id: 1,
    username: "Carlos",
    dni: "1234x",
    email: "carlos@gmail.com",
    rol: "alumno"
  }
  return res.json(user);
};

exports.new = (req, res, next) => {
  return res.json("false")
};

exports.delete = (req, res, next) => {
  return res.json("false");
}

exports.update = (req, res, next) => {
  return res.json("false");
};