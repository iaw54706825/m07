
var express = require("express");
var router = express.Router();


/** Home */
router.get("/", function(req, res, next) {
    res.render("index", {title: "Home"})
});

/** Incidencias */
router.get("/incidencias", function(req, res, next) {
    res.render("incidencias", {title: "Incidencias"})
});

/** ListaIncidencias */
router.get("/listaIncidencias", function(req, res, next) {
    let listaIncidencias = [
        {id: 1, tipoIncidencia: 'Otros', descripcion: 'Errores'},
        {id: 2, tipoIncidencia: 'Error', descripcion: 'Errores'},
        {id: 3, tipoIncidencia: 'Borrado', descripcion: 'Errores'}
    ];
    res.render("listaIncidencias", {incidencias: listaIncidencias})
});

/** Chat */
router.get("/chat", function(req, res, next) {
    res.render("chat")
});
module.exports = router;