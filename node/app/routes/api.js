var path = require("path");
var ctrlDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();
//Controladores
var userCtrl = require(path.join(ctrlDir, "user"));
var incidenciasCtrl = require(path.join(ctrlDir, "incidencias"));
var chatCtrl = require(path.join(ctrlDir, "chat"));


// Users
//Get all
router.get("/users", userCtrl.getAll);
//Get user by keyword(object)
router.get("/users/search", userCtrl.search);
//Get user by id
router.get("/users/:id", userCtrl.getUser);
//Insert user
router.post("/users/new", userCtrl.new);
//Delete user
router.delete("/users/:id", userCtrl.delete);
//Put user
router.put("/users/update", userCtrl.update);

// Incidencias
//Get all
router.get("/incidencias", incidenciasCtrl.getAll);
//Get user by keyword(object)
router.get("/incidencias/search", incidenciasCtrl.search);
//Get user by id
router.get("/incidencias/:id", incidenciasCtrl.getIncidencias);
//Insert user
router.post("/incidencias/new", incidenciasCtrl.new);
//Delete user
router.delete("/incidencias/:id", incidenciasCtrl.delete);
//Put user
router.put("/incidencias/update", incidenciasCtrl.update);

module.exports = router;