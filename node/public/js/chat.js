$(document).ready(function () {
    $('#enviar').click(enviar);
    $("#entradaChat").on('keyup', function (e) {
        if (e.key === 'Enter' || e.keyCode === 13) {
            enviar();
        }
    });
});

//Agregar chat
const enviar = () => {
    let text = $('#entradaChat').val();
    if (text.length > 1) {
        textFormato = document.createElement('span');
        textFormato.append(text);
        $(textFormato).addClass('bg-success rounded px-1');
        console.log(textFormato);
        $('#chat').append(textFormato);
        $('#chat').append('<br>');
        $('#entradaChat').val('');
    } else {
        $('#entradaChat').val('');
    }
}

//Activar pos actual enlace
$(document).ready(function () {
    let pathname = window.location.pathname;
    let urlComputed = "nav a[href='" + pathname + "']";
    $(urlComputed).addClass("active");
})
