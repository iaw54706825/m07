require("dotenv").config();
var port = process.env.SERVER_PORT;
var express = require("express");
var app = express();
var path = require("path");
//Layouts
const expressLayouts = require('express-ejs-layouts');

//Motor plantillas EJS y Layouts
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use(expressLayouts);

//css & js
app.use(express.static(__dirname + '/public'));

//Puerto
var server = require("http").createServer(app).listen(port, ()=> {
    console.log("Puerto conectado: " + port)
});

//Rutas
var indexRouter = require("./app/routes/base");
var api = require("./app/routes/api");
app.use('/', indexRouter);
app.use('/api',api);


//Error 404
app.use((req, res, next) => {
    res.status(404).sendFile(__dirname + '/views/404.html');
});

