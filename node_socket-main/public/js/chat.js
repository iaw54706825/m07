$(document).ready(function () {
  const socket = io();

  $("#send").submit(function (e) {
    e.preventDefault();
    var msg = $("#msg").val();
    if (msg) {
      socket.emit('message', msg);
      msg = '';
    }
  });

  socket.on('message', (msg) => {
    console.log(msg);
    $('#chat').append(msg);
    $('#chat').append('<br>');
  })
  //We listen on "newMsg" and react if we detect 
  //Acciones a realizar cuando se detecta actividad en el canal newMsg

});