## Comandos:
- Primero ejecutamos el script que esta en el forum.(no es necesario en principio)
- mkdir /sys/fs/cgroup/systemd
- mount -t cgroup -o none,name=systemd cgroup /sys/fs/cgroup/systemd 
- Luego ejecutamos el comando : `docker system prune -a`
- `docker-compose up -d`
- Seguidamente modificamos el archiv .env -> DB_PORT=3306 ,  (Al final del archivo .env) MSQL_PORT 3307 y PHPMYADMIN_PORT=99
- Instalamos extension docker al VisualStudio, y para ejecutar comandos sobre PHP, click derecho sobre php:7.4->atack shell
- Posibles problemas de permiso: `chmod 777 -R laravel_course-4.2/`

