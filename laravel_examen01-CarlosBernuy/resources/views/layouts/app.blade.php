<!--Use this file as layout of your hole APP-->
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">Start Bootstrap</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Register</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">

    <div class="row">

      <div class="col-lg-3">

        <h1 class="my-4">Shop Name</h1>
        <div class="list-group">
          <a href="categorias/category1" class="list-group-item">category 1</a>
          <a href="categorias/category2" class="list-group-item">category 2</a>
          <a href="categorias/category3" class="list-group-item">category 3</a>
        </div>
       
        
        <form>
        <div class="form-group">
          <div class="form-check">
          <input class="form-check-input" type="checkbox" value="1" id="defaultCheck1">
          <label class="form-check-label" for="defaultCheck1">
            1 estrella
          </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="2" id="defaultCheck1">
            <label class="form-check-label" for="defaultCheck">
              2 estrellas
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="3" id="defaultCheck1">
            <label class="form-check-label" for="defaultCheck">
              3 estrellas
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="4" id="defaultCheck1">
            <label class="form-check-label" for="defaultCheck">
              4 estrellas
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="5" id="defaultCheck1">
            <label class="form-check-label" for="defaultCheck">
              5 estrellas
            </label>
          </div>
          <!-- Poner 5 checkbox de 1 a 5 estrellas-->
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>

      </div>
      <!-- /.col-lg-3 -->
    @yield('content')
      
      <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
    </div>
    <!-- /.container -->
  </footer>


</body>

</html>