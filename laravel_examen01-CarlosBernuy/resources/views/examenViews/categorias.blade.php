@extends('layouts.app')
@section('content')
<div class="col-lg-9"> 

  @foreach($products as $prod)   
  <div class="col-lg-4 col-md-6 mb-4">
    <div class="card h-100">
      <a href="#"><img class="card-img-top" src="{{ asset('img/lego4.jpeg')}}" alt=""></a>
      <div class="card-body">
        <h4 class="card-title">
          <a href="#">{{ $prod->nombre}}</a>
        </h4>
        <h5>{{ $prod->precio}}</h5>
        <p class="card-text">{{ $prod->descripcion}}</p>
      </div>
      <div class="card-footer">
        <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
      </div>
    </div>
  </div>

  @endforeach
</div>
      <!-- /.col-lg-9 -->
@endsection

