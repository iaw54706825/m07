<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductSeeder::class);
    }
}
// $table->id();
// $table->string('nombre');
// $table->double('precio');
// $table->string('descripcion');
// $table->integer('puntuacion');        
// $table->string('categoria');
// $table->timestamps();
class ProductSeeder extends Seeder
{
    public function run(){
        DB::table('products')-> insert([
            [
                'nombre' => 'nombre1',
                'precio' => 10,
                'descripcion' => 'descripcionPrueba',
                'puntuacion' => 3,
                'categoria' => 'category1'                    
            ],
            [
                'nombre' => 'nombre2',
                'precio' => 20,
                'descripcion' => 'descripcionPrueba',
                'puntuacion' => 2,
                'categoria' => 'category2'                    
            ],
            [
                'nombre' => 'nombre3',
                'precio' => 30,
                'descripcion' => 'descripcionPrueba',
                'puntuacion' => 4,
                'categoria' => 'category3'                    
            ],
            [
                'nombre' => 'nombre4',
                'precio' => 40,
                'descripcion' => 'descripcionPrueba',
                'puntuacion' => 5,
                'categoria' => 'category1'                    
            ]
        ]);
    }
}