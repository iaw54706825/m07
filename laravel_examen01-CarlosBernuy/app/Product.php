<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function scopeCategory($query,$category) {
        return $query->where('categoria',$category);        
    }

    public function scopePuntuacion($query,$puntuacion) {
        return $query->where('puntuacion',$puntuacion);        
    }
}
