<?php
namespace App\Http\Controllers;
use App\User;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{    
   
    private $productModel;
    private $_filters;

    public function __construct()
    {
        /**
         * Filters (name=>value) format to show in the view
         * Write the content of the stars
         */
        $this->productModel = new Product();

        $this->_filters=(object)array(
            'category'=>array('Categori1'=>'cat1','Categori2'=>'cat3','Categori3'=>'cat3'),
            'stars'=>array()
        );
    }
    /**
     * Method to list all the products
     */
    public function all()
    {   
        $products = Product::get();        
        return view('examenViews/categorias',compact('products'));
    }

    /**
     * Method to list the products filtered by category
     */
    public function category($category)
    {   
        $query = Product::category($category);
        $products = $query->get();
        return view('examenViews/categorias',compact('products'));
    }

    /**
     * Method to list the products filtered by stars
     */
    public function stars(Request $r)
     {
    //     Esta opcion que comento serviria si la informacion
    //     de la categoria pasara por la misma request
        //$category = $r->get("categoria")
        // if (!empty($r)) {
        //     $query = Product::category($category);
        // }
        $query = Product::puntuacion($puntuacion);
        $products = $query->get();
        return view('examenViews/categorias',compact('products'));
    }
}