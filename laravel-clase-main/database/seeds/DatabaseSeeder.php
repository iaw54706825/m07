<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(ProductsSeeder::class);
    }
}

class ProductsSeeder extends Seeder {

    public function run()
    {
        DB::table('products')->insert(['name' => 'test', 'desc' => 'test2','price'=>10.02]);
    }

}
