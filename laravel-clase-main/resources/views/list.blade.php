@extends('layouts.app')

@section('title')
Lista de productos
@endsection

@section('content')
        <div class="flex-center position-ref ">
            <div class="content">
                <div class="title m-b-md">
                <ul class="list-group">
                    @forelse ($products as $product)
                    <li class="list-group-item">{{ $product->name }}</li>
                    @empty
                        <p>No Products</p>
                    @endforelse
                    </ul>
                </div>
            
            </div>
        </div>
    </body>
</html>
@endsection
