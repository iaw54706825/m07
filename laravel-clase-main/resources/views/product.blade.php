@extends('layouts.app')

@section('title')
Lista de productos
@endsection

@section('content')
<div class="flex-center position-ref ">
    <div class="content">
        <div class="title m-b-md">
        <h1>Hola {{$user}}</h1>
            <ul>
            @isset($product)
                <p>{{$product->name}}</p>
            @endisset
            </ul>
        </div>
    
    </div>
</div>
@endsection