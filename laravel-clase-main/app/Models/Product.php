<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function scopePrice($query,$price){
        return $query->where('price', '>', $price);
    }
}
