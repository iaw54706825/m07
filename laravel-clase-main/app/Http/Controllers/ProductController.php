<?php
namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Models\Product;

class ProductController extends Controller
{    
    /**
     *  Form Request PRO
     */
    private $pa;
    private $pb;
    private $products;

    public function __construct()
    {
        $this->pa=(object)array('name'=>'Champu','desc'=>'Para limpiarte','price'=>10.50);
        $this->pb=(object)array('name'=>'Rueda','desc'=>'Redonda','price'=>100.50);
        $this->products=array($this->pa,$this->pb,$this->pb,$this->pa,$this->pb,$this->pb);
    }
    public function list()
    {   
        /*$products = Product::cursor()->filter(function ($product) {
            return $product->price > 50;
        });;
        */
        $products = Product::price(30)->where('price','<','90')->get();
       /* $product = new Product;
        $product->name = 'Pastel Richos Style';
        $product->desc  = 'chessecake';
        $product->price  = 40.20;
        $product->save();
        */
        return view('list')->with('products',$products);
    }

    public function show($id,$user)
    {   
        $product = $this->products[$id];
        return view('product')->with(['product'=>$product,'user'=>$user]);
    }

    //This method recieve Form data
    public function update(Request $request,$id)
    {
        $name = $request->input('name');
        $this->products[$id]->name=$name;
        return $this->list();
        //return redirect()->route('products');
    }
}