<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class CompraController extends Controller
{

    
    public function carrito (Request $r) {
        $datos = $r->session()->get('carrito',[]);
        array_push($datos, $r->all()); //Esta funcion pilla todos los valores recibidos por el request en formato llave-valor
        $r->session()->put('carrito', $datos);
        return redirect(url()->previous());
    }


    public function main()
    {
        /*Recuerda el estado de la compra y redirige a la pantalla en la que el usuario estaba antes: resumen, envio o confirmar */
        return redirect('/compra/resumen');
    }
    /**
     * Method to show the resume of the products in the chart
     */
    public function resumen()
    {
        //Dummy: hay que cambiar la info por la información guardada en el carrito (session)
        $products = [
            // (object) [
            //     'name' => 'Lego 1', 'category' => 0, 'description' => 'Esto es la descripcion lego 1', 'price' => 10.02, 'image' => 'lego1.jpeg', 'rating' => 2
            // ]
        ];
        foreach(session()->get('carrito') as $param) {
            array_push($products,(object)$param);
        }
        return view('compra/resumen')
            ->with('products', $products);
    }

    /**
     * Method to show and process the shipping form (envio)
     */
    public function envio()
    {
        return view('compra/envio');
    }
    /**
     * Method to show and process the shipping form (envio)
     */
    public function verificarEnvio(CompraRequest $request)
    {
        $formOK = false;
        if ($request) {
            $formOK = true;
        }
        /* PONER AQUI TODO LO NECESARIO PARA VERIFICAR EL FORMULARIO */
        $mensaje = "";
        try{
            $service->uploadFile($r->file('imagen'));
            $user = new User;
            $user->name = $r['name'];
            $user->email = $r['email'];
            $user->direccion = $r['direccion'];
            $user->email = $r['email'];
            $user->save();
        }catch(QueryException $e){
            $mensaje = "Problema con la bbdd";
        }catch(EscrituaException $e){
            $mensaje = "problemas de escritura";
        }catch(InvelidURl $e) {
            $mensaje = "url invalida";
        }
        //si el formulario se ha rellenado correctamente se redirecciona a la pagina de confirmación
        if ($formOK) redirect('/compra/confirmar');
        return view('compra/envio');
    }
    /**
     * Method to show the list of procuts and shipping info
     */
    public function confirmar()
    {
        //Dummy: hay que cambiar la info por la información guardada en session
        $products = [
         
        ];
        foreach(session()->get('carrito') as $param) {
            array_push($products,(object)$param);
        }
        //Dummy: hay que cambiar la info por la información guardada en session
        $shipping =   (object)[
          
        ];

        foreach(session()->get('shipping') as $param) {
            array_push($shipping,(object)$param);
        }
        return view('compra/confirmar')->with('products', $products)->with('shipping', $shipping);
    }
}
