<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class CompraRequest extends FormRequest
{
    //protected $redirectRoute = 'post.create' //ruta definida en alguno de los archivos de la carpeta routes
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [   
            'name' => 'required',
            'email' => 'required|email:rfc,dns',
            'direccion' => 'required|min:4',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|min:6'
        ];
    }

    public function messages()
    {
        return [
           'name.required' => 'Introduce un nombre',
           'email.required' => 'Introduce un email',
           'direccion.required' => 'Introduce una direccion',
           'password.required' => 'Introduce una contraseña' ,
        ];
    }
    public function attributes()
    {
        return [
        ];
    }
    /**
     *  AJAX Response 
     */
    public function response(array $errors)
    {
        if ($this->expectsJson()) {
            return new JsonResponse($errors, 422);
        }
        return $this->redirector->to($this->getRedirectUrl())
            ->withInput($this->except($this->dontFlash))
            ->withErrors($errors, $this->errorBag);
    }
}
