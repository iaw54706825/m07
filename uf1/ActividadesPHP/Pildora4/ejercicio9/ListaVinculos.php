<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
</head>
<body>
    <?php
    class ListaVinculos {
        //Atributos
        private $lista = [];

        //Funciones
        public function cargarOpcion ($link) {
        $this->lista[] = $link;     
        }

        public function mostrar(){
            foreach($this->lista as $l) {
                echo $l . "<br>";
            }
        }
    }

    //creamos lista
    $lista = new ListaVinculos();
    $lista->cargarOpcion("www.google.com");
    $lista->cargarOpcion("www.marca.com");
    $lista->cargarOpcion("www.xataka.com");
    $lista->mostrar();
    ?>
</body>
</html>