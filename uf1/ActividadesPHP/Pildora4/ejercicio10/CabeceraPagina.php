<?php
    class CabeceraPagina {
        //Atributos
        private $titulo;
        private $posicion;
        private $colorFondo;
        private $fuente;

        //Funciones
        public function inicializar($titulo, $posicion, $colorFondo, $fuente) {
            $this->titulo = $titulo;
            $this->posicion = $posicion;
            $this->colorFondo = $colorFondo;
            $this->fuente = $fuente;
        }

        public function mostrar() {
            echo"Titulo: $this->titulo<br>
                 Posicion: $this->posicion<br>
                 Color de Fondo: $this->colorFondo<br>
                 Fuente: $this->fuente";
        }
    }

    $cabezera = new CabeceraPagina();
    $cabezera->inicializar("Titulo1", "Centro", "Azul", "Arial");
    $cabezera->mostrar();    
    ?>

<!DOCTYPE html>
<html lang="en">
<head>    
    <title>Document</title>
</head>
<body>

</body>
</html>