<html>
  <head>
    <title></title>
  </head>
  <body>
    <p>Total: 
    <?php 
    $precio = $_GET["precio"];
      //Funcion para calcular descuento
      function volumen($total) {
        //variable resultado
        $descuento;
        if ($total < 100) {
          $descuento = $total;
        } else if ($total >= 100 && $total <= 499.99) {
          $descuento = $total * 0.9;
        } else {
          $descuento = $total * 0.85;
        }
        return $descuento;
      }     
      //Introducimos como parametro el valor recibido del formulario
      echo volumen($precio);
     ?> 
     </p>
  </body>
</html>
