<html>
  <head>
    <title>PHP Test</title>
  </head>
  <body>
    <?php 
    class Empleado {
      //atributos
      private $nombre;
      private $sueldo;

      //Funciones
      public function inicializar ($nombre, $sueldo) {
        $this->nombre = $nombre;
        $this->sueldo = $sueldo;        
      }

      public function pagarImpuesto() {
        $r = " no debe pagar impuestos";
        if ($this->sueldo > 3000) {
          $r = " debe pagar impuestos";
        }
        echo $this->nombre . $r;
      }

    } 

    $empleado = new Empleado();
    $empleado->inicializar("Carlos",3000);
    $empleado->pagarImpuesto();
?>  
  </body>
</html>
