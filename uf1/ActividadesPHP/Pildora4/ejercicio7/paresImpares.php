<html>
  <head>
    <title>PHP Test</title>
  </head>
  <body>
  <h4>Ejercicio 7</h4>
  <p>El resultado es:</p>
  <?php
    $arrayNumeros = $_GET["num"];

    function separar($lista) {
    //Creamos arreglo para pares e impares
    $arrayParesImpares = [
      "pares" => [],
      "impares" => [],
    ];
      //Recorremos los numeros de la lista y condicionamos
      foreach($lista as $numero) {
        if ($numero % 2 == 0) {
          array_push($arrayParesImpares["pares"], $numero);
        } else {        
          array_push($arrayParesImpares["impares"], $numero);
        }
      }
      sort($arrayParesImpares["pares"]);
      sort($arrayParesImpares["impares"]);
    
      return $arrayParesImpares;
    }
    print_r(separar($arrayNumeros));
  ?>
  </body>
</html>
