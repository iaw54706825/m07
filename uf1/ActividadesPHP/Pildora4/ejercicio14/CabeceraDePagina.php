<?php
    class CabeceraPagina {
        //Atributos
        private $titulo;
        private $posicion;
        private $colorFondo;
        private $fuente;

        //Constructor
        public function __construct($titulo="Titulo", $posicion="center", $colorFondo="powderblue", $fuente="verdana")
        {
            $this->titulo = $titulo;
            $this->posicion = $posicion;
            $this->colorFondo = $colorFondo;
            $this->fuente = $fuente;
        }      
       
        public function graficar() {
            //graficamos a partir de los atributos
            $cabecera = "<h1 style = 'text-align:$this->posicion; background-color:$this->colorFondo; font-family:$this->fuente;'>$this->titulo</h1>";
            echo $cabecera;
        }
    }      
    ?>

<!DOCTYPE html>
<html lang="en">
<head>    
    <title>Document</title>
</head>
<body>
<?php
 $cabezera = new CabeceraPagina("HolaCaracola");
 $cabezera->graficar(); 
?>
</body>
</html>