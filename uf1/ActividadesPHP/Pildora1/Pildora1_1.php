<html>
  <head>
    <title>PHP Test</title>
  </head>
  <body>
    <h1>Pildora 1  </h1>
    <?php 
      //mostrar la informacion del servidor que lo ejecuta
      //phpinfo();

      //Question: Crea un script para muestre un texto de cabecera h1 y un parrafo por pantalla
      echo "<h1>Cabecera</h1> <p>parrafo</p>";

      //Question: Crea un saludo que cambie en función del nombre almacenado en una variable $name
      $nombre = "Carlos";
      echo "<h1>Hola $nombre</h1>";

      //Question: Haz un programa que realize en una misma sentencia un conjunto de diferentes operaciones aritmeticas utilizando tanto numero como valores almacenados en variables.
      $num1 = 5.2;
      $num2 = 4.4;
      $resultado = $num1 * 2 - ($num1 + $num2) + $num2;
      echo "Resultado " . $resultado;
    ?> 
  </body>
</html>
