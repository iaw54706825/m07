<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
       // Verificamos si se envio el formulario
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        //Aqui comprobamos que se subio el archivo y no hay errores
        if(isset($_FILES["archivo"]) && $_FILES["archivo"]["error"] == 0){
            $nombre = $_FILES["archivo"]["name"];
            $ubicacion = $_FILES["archivo"]["tmp_name"];
        }
    }
    //Comprobamos si el archivo ya existe en el directorio y subimos
    if(file_exists("subidas/" . $nombre)){
        echo $nombre . " ya existe<br>";
    } else{
        move_uploaded_file($ubicacion, "subidas/" . $nombre);
        echo "Tu archivo fue subido correctamente<br>";
    }
    
    //Listamos archivos existentes
    echo "<br><br>Lista de archivos:<br>";
    //Abrimos directorio y nos devuelve identificador
    $directorio = opendir("subidas");
        //A la funcion "readdir()" le pasamos el identificador del directorio
        while ($files = readdir($directorio)) {
            if ($files != "." && $files != "..") {
            echo "$files<br>";
            }
        }
    ?>
</body>
</html>